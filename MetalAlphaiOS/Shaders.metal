//
//  Shaders.metal
//  MetalIOSXAlpha
//
//  Created by Justin Gaussoin on 7/16/15.
//  Copyright © 2015 Justin Gaussoin. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

struct Uniforms {
    float4x4 modelMatrix;
    float4x4 projectionMatrix;
};

struct VertexIn {
    packed_float3 position;
    packed_float4 color;
};


struct VertexOut {
    float4 position [[position]];
    float4 color;
};


vertex VertexOut basic_vertex(const device VertexIn *vertex_array [[ buffer(0) ]],
                              const device Uniforms &uniforms [[ buffer(1) ]],
                              unsigned int vid [[ vertex_id ]]) {
    
    
    float4x4 mvMatrix = uniforms.modelMatrix;
    float4x4 pMatrix = uniforms.modelMatrix;
    
    VertexIn vin = vertex_array[vid];
    
    VertexOut vout;
    vout.position = pMatrix * mvMatrix * float4(vin.position, 1);
    vout.color = vin.color;
    return vout;
}

fragment half4 basic_fragment( VertexOut interpolated [[stage_in]]) { // 1
    return half4(interpolated.color[0], interpolated.color[1], interpolated.color[2], interpolated.color[3]);              // 2
}