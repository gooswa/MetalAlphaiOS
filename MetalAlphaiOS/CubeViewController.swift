//
//  CubeViewController.swift
//  MetalAlphaiOS
//
//  Created by Justin Gaussoin on 7/23/15.
//  Copyright © 2015 Justin Gaussoin. All rights reserved.
//

import UIKit
import MetalKit

class CubeViewController: UIViewController {

    var metalView:MainMetalView { return self.view as! MainMetalView }
    
    var device:MTLDevice!
    
    
    var objectToDraw:Triangle!
    var cubeToDraw:Cube!
    
    var projectionMatrix:Matrix4!
    
    var vertexBuffer:MTLBuffer!
    
    var pipelineState: MTLRenderPipelineState!
    
    var commandQueue:MTLCommandQueue!
    
    var shouldRender:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        device = MTLCreateSystemDefaultDevice()
        metalView.device = device
        metalView.colorPixelFormat = .BGRA8Unorm
        metalView.framebufferOnly = true
        metalView.delegate = self
        
        createBuffers()
        createPipeline()
        
        projectionMatrix = Matrix4.makePerspectiveViewAngle(Matrix4.degreesToRad(85.0), aspectRatio: Float(view.bounds.size.width/view.bounds.size.height), nearZ: 0.01, farZ: 100.0)
        
    }
    
    func createBuffers() {
        objectToDraw = Triangle(device: device)
        cubeToDraw = Cube(device: device)
        
//        cubeToDraw.positionX = 0.0
//        cubeToDraw.positionY =  0.0
//        cubeToDraw.positionZ = -2.0
//        cubeToDraw.rotationZ = Matrix4.degreesToRad(45)
          cubeToDraw.scale = 0.5
        
        //        cubeToDraw.positionX = -0.25
        //        cubeToDraw.rotationZ = Matrix4.degreesToRad(45);
        //        cubeToDraw.scale = 0.5
        //        cubeToDraw.positionY =  0.25
        //        cubeToDraw.positionZ = -0.25
    }
    
    
    func createPipeline() {
        let defaultLibrary = device.newDefaultLibrary()
        let fragmentProgram = defaultLibrary?.newFunctionWithName("basic_fragment")
        let vertexProgram = defaultLibrary?.newFunctionWithName("basic_vertex")
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexFunction = vertexProgram!
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = .BGRA8Unorm
        
        do {
            try pipelineState = device.newRenderPipelineStateWithDescriptor(pipelineStateDescriptor)
        } catch {
            print("Couldn't Create Pipeline State \(error)")
        }
        commandQueue = device.newCommandQueue()
        shouldRender = true
    }


}


extension CubeViewController : MTKViewDelegate {
    
    func drawInMTKView(view: MTKView) {
        if !shouldRender { return }
        let drawable = metalView.currentDrawable!
        
        cubeToDraw.render(commandQueue, pipelineState: pipelineState, drawable: drawable, projectionMatrix:projectionMatrix, clearColor: nil)
        
    }
    
    internal func mtkView(view: MTKView, drawableSizeWillChange size: CGSize) {
        
    }
}