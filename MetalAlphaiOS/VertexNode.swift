//
//  Vertex.swift
//  MetalIOSXAlpha
//
//  Created by Justin Gaussoin on 7/22/15.
//  Copyright © 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import MetalKit
import QuartzCore

struct Vertex {
    var x, y, z:Float
    var r, g, b, a:Float
    
    func floatBuffer() -> [Float] {
        return [x, y, z, r, g, b, a]
    }
}


class Node {
    let name:String
    var vertexCount:Int
    var vertexBuffer:MTLBuffer
    var uniformBuffer:MTLBuffer?
    var device:MTLDevice
    
    
    var positionX:Float = 0
    var positionY:Float = 0
    var positionZ:Float = 0
    
    var rotationX:Float = 0
    var rotationY:Float = 0
    var rotationZ:Float = 0
    
    var scale:Float = 1.0
    
    init (name:String, vertices:Array<Vertex>, device:MTLDevice) {
        var vertexData = Array<Float>()
        for vertex in vertices {
            vertexData += vertex.floatBuffer()
        }
        
        let dataSize = vertexData.count * sizeofValue(vertexData[0])
        vertexBuffer = device.newBufferWithBytes(vertexData, length: dataSize, options: MTLResourceOptions.OptionCPUCacheModeDefault)
        
        
        
        self.name = name
        self.device = device
        vertexCount = vertices.count
    }
    
    func modelMatrix() -> Matrix4 {
        let matrix = Matrix4()
        matrix.translate(positionX, y: positionY, z: positionZ)
        matrix.rotateAroundX(rotationX, y: rotationY, z: rotationZ)
        matrix.scale(scale, y: scale, z: scale)
        return matrix
    }
    
    
    func render(commandQueue:MTLCommandQueue, pipelineState:MTLRenderPipelineState, drawable:CAMetalDrawable, projectionMatrix:Matrix4, clearColor:MTLClearColor?) {
        print("\(self) \(__FUNCTION__)")
        let renderPassDescriptor = MTLRenderPassDescriptor()
        renderPassDescriptor.colorAttachments[0].texture = drawable.texture
        renderPassDescriptor.colorAttachments[0].loadAction = .Clear
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColor(red: 0.0, green: 104.0/255.0, blue: 5.0/255.0, alpha: 1.0)
        renderPassDescriptor.colorAttachments[0].storeAction = .Store
        
        let commandBuffer = commandQueue.commandBuffer()
        let renderEncoder = commandBuffer.renderCommandEncoderWithDescriptor(renderPassDescriptor)
        
        renderEncoder.setRenderPipelineState(pipelineState)
        renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, atIndex: 0)
        
        let nodeModelMatrix = modelMatrix()
        //uniformBuffer = device.newBufferWithLength(sizeof(Float) * Matrix4.numberOfElements(), options: MTLResourceOptions.OptionCPUCacheModeDefault)
        
        uniformBuffer = device.newBufferWithLength(sizeof(Float) * Matrix4.numberOfElements() * 2, options: MTLResourceOptions.OptionCPUCacheModeDefault)
        
        let bufferPointer = uniformBuffer?.contents()
        
        memcpy(bufferPointer!, nodeModelMatrix.raw(), sizeof(Float) * Matrix4.numberOfElements())
        memcpy(bufferPointer! + sizeof(Float)*Matrix4.numberOfElements(), projectionMatrix.raw(), sizeof(Float)*Matrix4.numberOfElements())
        
        renderEncoder.setVertexBuffer(uniformBuffer, offset: 0, atIndex: 1)
        
        renderEncoder.drawPrimitives(.Triangle, vertexStart: 0, vertexCount: vertexCount, instanceCount: vertexCount/3)
        renderEncoder.endEncoding()
        
        commandBuffer.presentDrawable(drawable)
        commandBuffer.commit()
    }
}


class Triangle : Node {
    init (device:MTLDevice) {
        let v0 = Vertex(x: 0, y: 1, z: 0, r: 1, g: 0, b: 0, a: 1)
        let v1 = Vertex(x: -1, y: -1, z: 0, r: 0, g: 1, b: 0, a: 1)
        let v2 = Vertex(x: 1, y: -1, z: 0, r: 0, g: 0, b: 1, a: 1)
        
        let verticesArray = [v0, v1, v2]
        super.init(name: "Triangle", vertices: verticesArray, device: device)
    }
}




class Cube : Node {
    
    
    init (device:MTLDevice) {
        
        let a = Vertex(x: -1, y: 1, z: 1, r: 1, g: 0, b: 0, a: 1)
        let b = Vertex(x: -1, y: -1, z: 1, r: 0, g: 1, b: 0, a: 1)
        let c = Vertex(x: 1, y: -1, z: 1, r: 0, g: 0, b: 1, a: 1)
        let d = Vertex(x: 1, y: 1, z: 1, r: 0.1, g: 0.6, b: 0.4, a: 1)
        
        let q = Vertex(x: -1, y: 1, z: -1, r: 1, g: 0, b: 0, a: 1)
        let r = Vertex(x: 1, y: 1, z: -1, r: 0, g: 1, b: 0, a: 1)
        let s = Vertex(x: -1, y: -1, z: -1, r: 0, g: 0, b: 1, a: 1)
        let t = Vertex(x: 1, y: -1, z: -1, r: 0.1, g: 0.6, b: 0.4, a: 1)
        
//        let a = Vertex(x: -0.3, y:   0.3, z:   0.3, r:  1.0, g:  0.0, b:  0.0, a:  1.0)
//        let b = Vertex(x: -0.3, y:  -0.3, z:   0.3, r:  0.0, g:  1.0, b:  0.0, a:  1.0)
//        let c = Vertex(x:  0.3, y:  -0.3, z:   0.3, r:  0.0, g:  0.0, b:  1.0, a:  1.0)
//        let d = Vertex(x:  0.3, y:   0.3, z:   0.3, r:  0.1, g:  0.6, b:  0.4, a:  1.0)
//        
//        let q = Vertex(x: -0.3, y:   0.3, z:  -0.3, r:  1.0, g:  0.0, b:  0.0, a:  1.0)
//        let r = Vertex(x:  0.3, y:   0.3, z:  -0.3, r:  0.0, g:  1.0, b:  0.0, a:  1.0)
//        let s = Vertex(x: -0.3, y:  -0.3, z:  -0.3, r:  0.0, g:  0.0, b:  1.0, a:  1.0)
//        let t = Vertex(x:  0.3, y:  -0.3, z:  -0.3, r:  0.1, g:  0.6, b:  0.4, a:  1.0)
//        
        let verticesArray = [
            a, b, c, a, c, d,
            r, t, s, q, r, s,
            q, s, b, q, b, a,
            d, c, t, d, t, r,
            q, a, d, q, d, r,
            b, s, t, b, t, c
        ]
        super.init(name: "Cube", vertices: verticesArray, device: device)
    }
    
}

